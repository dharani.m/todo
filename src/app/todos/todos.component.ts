import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos = [];
  inputTodo: string = '';
  update = {
    forUpdate:false,
    id:0
  };
  buttonName: String = "Add";

  constructor() { }

  ngOnInit(): void { }

  sortTodo():void {
    this.todos.sort((a,b):number =>{
      if(a.completed == false && b.completed == true)
        return -1;
      else
        return 0;
    });
  }

  toggleCompleted(id: number): void {
    this.todos[id].completed = !this.todos[id].completed;
    this.sortTodo();
  }

  deleteTodo(id: number): void {
    this.todos = this.todos.filter((x, i) => i != id);
  }
  addTodo(): void {
    if(this.update.forUpdate){
      console.log("inside");
      this.todos[this.update.id].content = this.inputTodo;
      this.buttonName = "Add";
      this.inputTodo="";
      this.update.forUpdate = false;
    }else{
      this.todos.push({
        "content": this.inputTodo,
        "completed": false
      });
      this.inputTodo = '';
      this.sortTodo();
    }

  }
  updateTodo(i){
    console.log("called");
    this.update.forUpdate = true,
    this.update.id = i;
    this.inputTodo = this.todos[i].content;
    this.buttonName = "Update"
  }
}
